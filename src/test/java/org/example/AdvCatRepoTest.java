package org.example;

import org.example.data.AdvancedCatRepository;
import org.example.model.Cat;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class AdvCatRepoTest {
    private static final String DB_URL = "jdbc:h2:mem:test";
    private static final String DB_DRIVER = "org.h2.Driver";
    private final AdvancedCatRepository advancedCatRepository = new AdvancedCatRepository();
    Cat cat1 = new Cat(1, "Ramzes", 4, true);
    Cat cat2 = new Cat(2, "Murzik", 6, false);
    Cat cat3 = new Cat(3, "Matroskin", 2, true);
    @Before
    public void setUp() {
        advancedCatRepository.create(cat1);
        advancedCatRepository.create(cat2);
        advancedCatRepository.create(cat3);
    }
    @After
    public void tearDown(){
        try {
            Class.forName(DB_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL);
            Statement statement = connection.createStatement();
            statement.execute("DROP TABLE cats");
            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void shouldInsertCatsInDB()
    {
        assertTrue(advancedCatRepository.create(cat1));
        assertTrue(advancedCatRepository.create(cat2));
        assertTrue(advancedCatRepository.create(cat3));
    }
    @Test
    public void shouldReadFromDB(){
        assertEquals(cat1, advancedCatRepository.read(1L));
        assertEquals(cat2, advancedCatRepository.read(2L));
        assertEquals(cat3, advancedCatRepository.read(3L));
    }
    @Test
    public void shouldUpdateDB(){
        assertEquals(1, advancedCatRepository.update(1L, cat3));
        advancedCatRepository.findAll();
        assertNull(advancedCatRepository.read(1L));
        assertEquals(cat3, advancedCatRepository.read(3L));
        assertEquals(cat2, advancedCatRepository.read(2L));
    }
    @Test
    public void shouldDeleteFromDB(){
        advancedCatRepository.delete(2L);
        assertNull(advancedCatRepository.read(2L));
        assertNotNull(advancedCatRepository.read(1L));
        assertNotNull(advancedCatRepository.read(3L));
    }
    @Test
    public void shouldFindAllFromDB(){
        List<Cat> result= advancedCatRepository.findAll();
        assertEquals(cat1, result.get(0));
        assertEquals(cat2, result.get(1));
        assertEquals(cat3, result.get(2));
    }
}
