package org.example;

import org.example.config.SpringConfig;
import org.example.data.SpringCatRepository;
import org.example.model.Cat;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class SprCatRepoTest {
    private static final String DB_URL = "jdbc:h2:mem:test";
    private static final String DB_DRIVER = "org.h2.Driver";
    private static SpringCatRepository springCatRepository;
    Cat cat1 = new Cat(1, "Ramzes", 4, true);
    Cat cat2 = new Cat(2, "Murzik", 6, false);
    Cat cat3 = new Cat(3, "Matroskin", 2, true);

    @BeforeClass
    public static void setUp(){
        ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
        springCatRepository = context.getBean(SpringCatRepository.class);
    }

    @Before
    public void prepare() {
        springCatRepository.init();
        springCatRepository.create(cat1);
        springCatRepository.create(cat2);
        springCatRepository.create(cat3);
    }
    @After
    public void tearDown(){
        try {
            Class.forName(DB_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL);
            Statement statement = connection.createStatement();
            statement.execute("DROP TABLE cats");
            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void shouldInsertCatsInDB()
    {
        assertTrue(springCatRepository.create(cat1));
        assertTrue(springCatRepository.create(cat2));
        assertTrue(springCatRepository.create(cat3));
    }
    @Test
    public void shouldReadFromDB(){
        assertEquals(cat1, springCatRepository.read(1L));
        assertEquals(cat2, springCatRepository.read(2L));
        assertEquals(cat3, springCatRepository.read(3L));
    }
    @Test
    public void shouldUpdateDB(){
        assertEquals(1, springCatRepository.update(1L, cat3));
        springCatRepository.findAll();
        assertNull(springCatRepository.read(1L));
        assertEquals(cat3, springCatRepository.read(3L));
        assertEquals(cat2, springCatRepository.read(2L));
    }
    @Test
    public void shouldDeleteFromDB(){
        springCatRepository.delete(2L);
        assertNull(springCatRepository.read(2L));
        assertNotNull(springCatRepository.read(1L));
        assertNotNull(springCatRepository.read(3L));
    }
    @Test
    public void shouldFindAllFromDB(){
        List<Cat> result= springCatRepository.findAll();
        assertEquals(cat1, result.get(0));
        assertEquals(cat2, result.get(1));
        assertEquals(cat3, result.get(2));
    }
}

