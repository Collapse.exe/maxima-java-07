package org.example.data;

import org.example.model.Cat;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SimpleCatRepository implements BaseRepository<Cat, Long> {
    private static final String DB_URL = "jdbc:h2:mem:test";
    private static final String DB_DRIVER = "org.h2.Driver";

    public SimpleCatRepository() {
        try {
            Class.forName(DB_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL);
            System.out.println("Cоздаю таблицу.");
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE cats (Id BIGINT, Name VARCHAR(30), Weight TINYINT, isAngry BIT)");
            /*connection.close();*/
            System.out.println("Таблица создана.");

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean create(Cat element) {
        try {
            Class.forName(DB_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL);
            System.out.println("Соединение выполнено. Помещаю кота в таблицу");
            String request = String.format("INSERT INTO cats VALUES ('%d','%s','%d','%b')",
                    element.getId(), element.getName(), element.getWeight(), element.isAngry());
            Statement statement = connection.createStatement();
            statement.execute(request);
            connection.close();
            System.out.println("Соединение с БД закрыто. Кот добавлен.");
            return true;

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return false;
        }
    }


    @Override
    public Cat read(Long id) {
        try {
            Class.forName(DB_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL);
            System.out.println("Соединение для чтения выполнено.");
            Statement statement = connection.createStatement();
            String request = String.format ("SELECT * FROM cats WHERE Id = %d", id);
            ResultSet resultSet = statement.executeQuery(request);
            resultSet.next();
            String isAngry = resultSet.getBoolean("isAngry") ? "Сердитый" : "Добрый";
            System.out.printf("%s кот %s весом %s кг.%n", isAngry, resultSet.getString("Name"), resultSet.getString("Weight"));
            Cat cat = new Cat(resultSet.getLong("Id"), resultSet.getString("Name"), resultSet.getInt("Weight"), resultSet.getBoolean("isAngry"));
            connection.close();
            return cat;
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("SQL exception. Данные отсутствуют.");
            return null;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Класс не найден.");
            return null;
        }
    }

    @Override
    public int update(Long id, Cat element) {
        try {
            Class.forName(DB_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL);
            System.out.println("Соединение для обновления выполнено.");
            String request = String.format("UPDATE cats SET Id = '%d', Name = '%s', Weight = '%d', isAngry = '%b' WHERE Id = %d",
                    element.getId(), element.getName(), element.getWeight(), element.isAngry(), id);
            Statement statement = connection.createStatement();
            int rows = statement.executeUpdate(request);
            connection.close();
            System.out.println("Обновление успешно. Соединение закрыто.");
            return rows;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public void delete(Long id) {
        try {
            Class.forName(DB_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL);
            Statement statement = connection.createStatement();
            System.out.println("Соединение для удаления выполнено.");
            String request = String.format ("DELETE FROM cats WHERE Id = %d", id);
            statement.executeUpdate(request);
            System.out.println("Запись удалена.");
            statement.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Cat> findAll() {
        try {
            Class.forName(DB_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL);
            Statement statement = connection.createStatement();
            List<Cat> result= new ArrayList<>();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM cats");
            while (resultSet.next()){
                String isAngry = resultSet.getBoolean("isAngry") ? "Сердитый" : "Добрый";
                System.out.printf("%s кот %s весом %s кг.%n", isAngry, resultSet.getString("Name"), resultSet.getString("Weight"));
                Cat cat = new Cat(resultSet.getLong("Id"), resultSet.getString("Name"), resultSet.getInt("Weight"), resultSet.getBoolean("isAngry"));
                result.add(cat);
            }
            connection.close();
            return result;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
