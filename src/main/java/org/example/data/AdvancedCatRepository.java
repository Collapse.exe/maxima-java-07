package org.example.data;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.example.model.Cat;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.function.Function;

public class AdvancedCatRepository implements BaseRepository<Cat, Long> {
    private DataSource dataSource;



    public AdvancedCatRepository() {
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String propertiesPath = rootPath + "db.properties";

        try {
            Properties dbProps = new Properties();
            dbProps.load(new FileInputStream(propertiesPath));
            HikariConfig config = new HikariConfig();
            config.setJdbcUrl(dbProps.getProperty("db.url"));
            config.setDriverClassName(dbProps.getProperty("db.driver"));
            this.dataSource = new HikariDataSource(config);
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            System.out.println("Cоздаю таблицу.");
            statement.executeUpdate("CREATE TABLE cats (Id BIGINT, Name VARCHAR(30), Weight TINYINT, isAngry BIT)");
            connection.close();
            System.out.println("Таблица создана.");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean create(Cat element) {
        String request = "INSERT INTO cats VALUES (?, ?, ?, ?)";
        try {
            Connection connection = dataSource.getConnection();
            System.out.println("Соединение выполнено. Помещаю кота в таблицу.");

            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.setLong(1, element.getId());
            preparedStatement.setString(2, element.getName());
            preparedStatement.setInt(3, element.getWeight());
            preparedStatement.setBoolean(4, element.isAngry());
            preparedStatement.execute();

            System.out.println("Соединение с БД закрыто. Кот добавлен.");
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Cat read(Long id) {
        String request = "SELECT * FROM cats WHERE Id = ?";
        try {
            Connection connection = dataSource.getConnection();
            System.out.println("Соединение для чтения выполнено.");

            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();

            Function<ResultSet, Cat> catRowMapper = result -> {
                try {
                    result.next();
                    return new Cat(
                            resultSet.getLong("Id"),
                            resultSet.getString("Name"),
                            resultSet.getInt("Weight"),
                            resultSet.getBoolean("isAngry")
                    );
                } catch (SQLException e) {
                    System.out.println("Ошибка rowMapper.");
                    e.printStackTrace();
                    return null;
                }
            };
            return catRowMapper.apply(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int update(Long id, Cat element) {
        String request = "UPDATE cats SET Id = ?, Name = ?, Weight = ?, isAngry = ? WHERE Id = ?";
        try {
            Connection connection = dataSource.getConnection();
            System.out.println("Соединение для обновления выполнено.");

            PreparedStatement preparedStatement = connection.prepareStatement(request);
            preparedStatement.setLong(1, element.getId());
            preparedStatement.setString(2, element.getName());
            preparedStatement.setInt(3, element.getWeight());
            preparedStatement.setBoolean(4, element.isAngry());
            preparedStatement.setLong(5, id);
            int rows = preparedStatement.executeUpdate();
            System.out.println("Обновление успешно. Соединение закрыто.");
            return rows;
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public void delete(Long id) {
        String request = "DELETE FROM cats WHERE Id = ?";
        try {
            Connection connection = dataSource.getConnection();
            System.out.println("Соединение для удаления выполнено.");

            PreparedStatement statement = connection.prepareStatement(request);
            statement.setLong(1, id);
            statement.execute();
            System.out.println("Запись удалена.");

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Cat> findAll() {

        ResultSet resultSet;
        List<Cat> cats= new ArrayList<>();
        try {
            Connection connection = dataSource.getConnection();
            String request = "SELECT * FROM cats";
            PreparedStatement preparedStatement = connection.prepareStatement(request);
            resultSet = preparedStatement.executeQuery();
        } catch (SQLException e) {
            System.out.println("Не удалось подключиться к БД!");
            throw new RuntimeException(e);
        }

        Function<ResultSet, Cat> catRowMapper = result -> {
            try {
                return new Cat(
                        resultSet.getLong("Id"),
                        resultSet.getString("Name"),
                        resultSet.getInt("Weight"),
                        resultSet.getBoolean("isAngry")
                );
            } catch (SQLException e) {
                throw new RuntimeException("Ошибка rowMapper.");
            }
        };
        while (true){
            try {
                if (!resultSet.next()) break;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            cats.add(catRowMapper.apply(resultSet));
        }
        return cats;
    }
}
