package org.example.data;

import org.example.model.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SpringCatRepository implements BaseRepository<Cat, Long> {

    @Autowired private DataSource dataSource;
    @Autowired private RowMapper<Cat> catRowMapper;
    private JdbcTemplate jdbcTemplate;

    @PostConstruct
    public void init() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.execute("CREATE TABLE IF NOT EXISTS cats (Id BIGINT, Name VARCHAR(30), Weight TINYINT, isAngry BIT)");
    }

    @Override
    public boolean create(Cat element) {
        return jdbcTemplate.update("INSERT INTO cats VALUES (?, ?, ?, ?)",
                element.getId(),
                element.getName(),
                element.getWeight(),
                element.isAngry())>0;
    }

    @Override
    public Cat read(Long id) {
        String request = "SELECT * FROM cats WHERE Id = ?";
        /*return jdbcTemplate.query("SELECT * FROM cats WHERE Id = ?", new BeanPropertyRowMapper<>(Cat.class), id).get(0);*/
        List<Cat> result = jdbcTemplate.query(request, catRowMapper, id);
        return result.size()<1 ? null : result.get(0);
    }

    @Override
    public int update(Long id, Cat element) {
        return jdbcTemplate.update("UPDATE cats SET Id = ?, Name = ?, Weight = ?, isAngry = ? WHERE Id = ?",
                element.getId(),
                element.getName(),
                element.getWeight(),
                element.isAngry(), id);
    }

    @Override
    public void delete(Long id) {
        jdbcTemplate.update("DELETE FROM cats WHERE Id = ?", id);
    }

    @Override
    public List<Cat> findAll() {
        return new ArrayList<>(jdbcTemplate.query("SELECT * FROM cats", catRowMapper));
    }
}
