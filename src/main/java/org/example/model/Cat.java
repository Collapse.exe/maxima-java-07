package org.example.model;

import java.util.Objects;

public class Cat {
    private Long id;
    private String name;
    private int weight;
    private boolean isAngry;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isAngry() {
        return isAngry;
    }

    public void setAngry(boolean angry) {
        isAngry = angry;
    }

    public Cat(long id, String name, int weight, boolean isAngry) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.isAngry = isAngry;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", weight=" + weight +
                ", isAngry=" + isAngry +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cat)) return false;
        Cat cat = (Cat) o;
        return getWeight() == cat.getWeight() && isAngry() == cat.isAngry() && Objects.equals(getId(), cat.getId()) && Objects.equals(getName(), cat.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getWeight(), isAngry());
    }
}
